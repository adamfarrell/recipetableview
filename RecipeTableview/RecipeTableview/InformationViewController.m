//
//  InformationViewController.m
//  RecipeTableview
//
//  Created by Adam Farrell on 5/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "InformationViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    float width = self.view.frame.size.width;
    float height = self.view.frame.size.height;
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView* img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 80, width, height / 4)];
    [img setContentMode:UIViewContentModeScaleAspectFit];
    [img setImage:[UIImage imageNamed:[self.informationDictionary objectForKey:@"image"]]];
    [self.view addSubview:img];
    
    
    UILabel* title = [[UILabel alloc]initWithFrame:CGRectMake(0, (height / 4) + 90, self.view.frame.size.width, 50)];
    title.text = [self.informationDictionary objectForKey:@"title"];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor blackColor];
    title.numberOfLines = 1;
    title.adjustsFontSizeToFitWidth = YES;
    [title setFont:[UIFont boldSystemFontOfSize:24]];
    [self.view addSubview:title];
    
    UITextView* description = [[UITextView alloc]initWithFrame:CGRectMake(20, (height / 4) + 150, width - 40, height / 2)];
    [description setText:[self.informationDictionary objectForKey:@"description"]];
    [self.view addSubview:description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
