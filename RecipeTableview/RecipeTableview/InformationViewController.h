//
//  InformationViewController.h
//  RecipeTableview
//
//  Created by Adam Farrell on 5/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController

@property (nonatomic, strong) NSDictionary* informationDictionary;

@end
